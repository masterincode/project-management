import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { NavigationGuard } from '../gaurd/navigation.guard';
import { SharedModule } from './shared.module';
import { ProductListComponent } from '../component/product-list/product-list.component';
import { ConvertToSpacesPipe } from '../pipe/convert-to-spaces.pipe';
import { ProductDetailComponent } from '../component/product-detail/product-detail.component';
@NgModule({
  declarations: [
    ProductListComponent,
    ConvertToSpacesPipe,
    ProductDetailComponent, 
  ],
  imports: [
    RouterModule.forChild([
      { path: 'products', component: ProductListComponent },
      {
        path: 'products/:id',
        canActivate: [NavigationGuard],
        component: ProductDetailComponent
      },
    ]),
    SharedModule
  ]
})
export class ProductModule {
}


